REGISTERS_LIST = ["ax", "ah", "al", "bx", "bh", "bl", "cx", "ch", "cl", "dx", "dh", "dl"]
SMALL_REGISTERS_LIST = ["ah", "al", "bh", "bl", "ch", "cl", "dh", "dl"]
LARGE_REGISTERS_LIST = ["ax", "bx", "cx", "dx"]
